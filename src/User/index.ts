/* eslint-disable @typescript-eslint/camelcase */
import * as Yup from 'yup';

export const deleteUserValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
});

export const createUserByEmailInviteValidationSchema = (UserRole: any) =>
  Yup.object().shape({
    firstname: Yup.string().required(),
    lastname: Yup.string().required(),
    email: Yup.string().required(),
    userRole: Yup.string()
      .oneOf(Object.keys(UserRole))
      .required(),
  });

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;

export const createUserValidationSchema = Yup.object().shape({
  firstname: Yup.string()
    .required()
    .min(2),
  lastname: Yup.string()
    .required()
    .min(2),
  birthdate: Yup.date()
    .min(new Date(1900, 1, 1), 'You are not that old')
    .test('DOB', 'You must be at least 18 years old', value => {
      const dateOfBirth = new Date(value);
      const dateNow = new Date();

      if (dateNow.getFullYear() - dateOfBirth.getFullYear() < 18) {
        return false;
      } else {
        return true;
      }
    })
    .required('Please specify your birth date'),
  telephone: Yup.string()
    .min(2, 'telephone must be at least 2 characters')
    .max(30, 'telephone must be at most 20 characters')
    .matches(phoneRegExp, 'Phone number is not valid')
    .required('telephone must be at least 2 characters'),
  telephone_alt: Yup.string()
    .min(2, 'telephone must be at least 2 characters')
    .max(30, 'telephone must be at most 20 characters')
    .matches(phoneRegExp, 'Phone number is not valid'),
});

export const updateUserValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  user_title: Yup.string().notRequired(),
  firstname: Yup.string().notRequired(),
  middlename: Yup.string().notRequired(),
  lastname: Yup.string().notRequired(),
  username: Yup.string().notRequired(),
  preferredname: Yup.string().notRequired(),
  gender: Yup.string().notRequired(),
  nationality: Yup.number().notRequired(),
  birthdate: Yup.date()
    .min(new Date(1900, 1, 1), 'You are not that old')
    .test('DOB', 'You must be at least 18 years old', value => {
      const dateOfBirth = new Date(value);
      const dateNow = new Date();

      if (dateNow.getFullYear() - dateOfBirth.getFullYear() < 18) {
        return false;
      } else {
        return true;
      }
    })
    .notRequired(),
  organisation: Yup.number().notRequired(),
  department: Yup.string().notRequired(),
  position: Yup.string().notRequired(),
  email: Yup.string().notRequired(),
  telephone: Yup.string()
    .min(2, 'telephone must be at least 2 characters')
    .max(30, 'telephone must be at most 20 characters')
    .matches(phoneRegExp, 'Phone number is not valid')
    .notRequired(),
  telephone_alt: Yup.string()
    .min(2, 'telephone must be at least 2 characters')
    .max(30, 'telephone must be at most 20 characters')
    .matches(phoneRegExp, 'Phone number is not valid'),
  placeholder: Yup.bool().notRequired(),
  roles: Yup.array()
    .of(Yup.number())
    .notRequired(),
  orcid: Yup.string().notRequired(),
  refreshToken: Yup.string().notRequired(),
});

export const updateUserRolesValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  roles: Yup.array()
    .of(Yup.number())
    .required(),
});

export const signInValidationSchema = Yup.object().shape({
  email: Yup.string().email(),
  password: Yup.string()
    .min(8, 'Password must be at least 8 characters')
    .max(25, 'Password must be at most 25 characters')
    .required('Password must be at least 8 characters'),
});

export const getTokenForUserValidationSchema = Yup.object().shape({
  userId: Yup.number().required(),
});

export const resetPasswordByEmailValidationSchema = Yup.object().shape({
  email: Yup.string()
    .email('Please enter a valid email')
    .required('Please enter an email'),
});

export const addUserRoleValidationSchema = Yup.object().shape({
  userID: Yup.number().required(),
  roleID: Yup.number().required(),
});

export const updatePasswordValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  password: Yup.string().required(),
});

export const userPasswordFieldBEValidationSchema = Yup.object().shape({
  password: Yup.string()
    .required(
      'Password must contain at least 8 characters (including upper case, lower case and numbers)'
    )
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
      'Password must contain at least 8 characters (including upper case, lower case and numbers)'
    ),
  token: Yup.string().required(),
});

export const userPasswordFieldValidationSchema = Yup.object().shape({
  password: Yup.string()
    .required(
      'Password must contain at least 8 characters (including upper case, lower case and numbers)'
    )
    .matches(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,
      'Password must contain at least 8 characters (including upper case, lower case and numbers)'
    ),
  confirmPassword: Yup.string()
    .required()
    .label('Confirm password')
    .test('passwords-match', 'Passwords must match', function(value) {
      return this.parent.password === value;
    }),
});
